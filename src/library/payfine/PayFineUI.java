package library.payfine;

import java.util.Scanner;

public class PayFineUI {

	public static enum UiState {
		INITIALISED, READY, PAYING, COMPLETED, CANCELLED
	};

	private pAY_fINE_cONTROL payFineControl;
	private Scanner input;
	private UiState uiState;

	public PayFineUI(pAY_fINE_cONTROL payFineControl) {
		this.payFineControl = payFineControl;
		input = new Scanner(System.in);
		uiState = UiState.INITIALISED;
		payFineControl.SeT_uI(this);
	}

	public void SeT_StAtE(UiState uiState) {
		this.uiState = uiState;
	}

	public void RuN() {
		output("Pay Fine Use Case UI\n");

		while (true) {

			switch (uiState) {

				case READY:
					String memberDetail = input("Swipe member card (press <enter> to cancel): ");
					if (memberDetail.length() == 0) {
						payFineControl.CaNcEl();
						break;
					}
					try {
						int memberId = Integer.valueOf(memberDetail).intValue();
						payFineControl.CaRd_sWiPeD(memberId);
					} catch (NumberFormatException e) {
						output("Invalid memberId");
					}
					break;

				case PAYING:
					double amountPaying = 0;
					String amountString = input("Enter amount (<Enter> cancels) : ");
					if (amountString.length() == 0) {
						payFineControl.CaNcEl();
						break;
					}
					try {
						amountPaying = Double.valueOf(amountString).doubleValue();
					} catch (NumberFormatException e) {
					}
					if (amountPaying <= 0) {
						output("Amount must be positive");
						break;
					}
					payFineControl.PaY_FiNe(amountPaying);
					break;

				case CANCELLED:
					output("Pay Fine process cancelled");
					return;

				case COMPLETED:
					output("Pay Fine process complete");
					return;

				default:
					output("Unhandled state");
					throw new RuntimeException("FixBookUI : unhandled state :" + uiState);

			}
		}
	}

	private String input(String prompt) {
		System.out.print(prompt);
		return input.nextLine();
	}

	private void output(Object object) {
		System.out.println(object);
	}

	public void DiSplAY(Object object) {
		output(object);
	}

}
