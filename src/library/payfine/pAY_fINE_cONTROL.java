package library.payfine;

import library.entities.Library;
import library.entities.Member;

public class pAY_fINE_cONTROL {

	private PayFineUI payFineUI;

	private enum ControlState {
		INITIALISED, READY, PAYING, COMPLETED, CANCELLED
	};

	private ControlState state;

	private Library library;
	private Member member;

	public pAY_fINE_cONTROL() {
		this.library = Library.GeTiNsTaNcE();
		state = ControlState.INITIALISED;
	}

	public void SeT_uI(PayFineUI payFineUI) {
		if (!state.equals(ControlState.INITIALISED)) {
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}
		this.payFineUI = payFineUI;
		payFineUI.SeT_StAtE(PayFineUI.UiState.READY);
		state = ControlState.READY;
	}

	public void CaRd_sWiPeD(int MeMbEr_Id) {
		if (!state.equals(ControlState.READY))
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");

		member = library.gEt_MeMbEr(MeMbEr_Id);

		if (member == null) {
			payFineUI.DiSplAY("Invalid Member Id");
			return;
		}
		payFineUI.DiSplAY(member.toString());
		payFineUI.SeT_StAtE(PayFineUI.UiState.PAYING);
		state = ControlState.PAYING;
	}

	public void CaNcEl() {
		payFineUI.SeT_StAtE(PayFineUI.UiState.CANCELLED);
		state = ControlState.CANCELLED;
	}

	public double PaY_FiNe(double AmOuNt) {
		if (!state.equals(ControlState.PAYING))
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");

		double ChAnGe = member.PaY_FiNe(AmOuNt);
		if (ChAnGe > 0)
			payFineUI.DiSplAY(String.format("Change: $%.2f", ChAnGe));

		payFineUI.DiSplAY(member.toString());
		payFineUI.SeT_StAtE(PayFineUI.UiState.COMPLETED);
		state = ControlState.COMPLETED;
		return ChAnGe;
	}

}
